<?php

use Latte\Runtime as LR;

/** source: templates/bmiTemplate.latte */
final class Template3c262b6a64 extends Latte\Runtime\Template
{
	public const Source = 'templates/bmiTemplate.latte';


	public function main(array $ʟ_args): void
	{
		extract($ʟ_args);
		unset($ʟ_args);

		echo '<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Výpočet BMI</title>
    
    
</head>
<body>

<h1>Výpočet BMI</h1>


<form method="post" action="bmi.php">
    <label for="hmotnost">Hmotnost (kg):</label>
    <input type="number" id="hmotnost" name="hmotnost" required><br><br>
    
    <label for="vyska">Výška (m):</label>
    <input type="number" step="0.01" id="vyska" name="vyska" required><br><br>
    
    <label for="vek">Věk:</label>
    <input type="number" id="vek" name="vek" required><br><br>
    
    <input type="submit" value="Vypočítat BMI">
</form>

<div>
tvoje bmi je ';
		echo LR\Filters::escapeHtmlText($bmi) /* line 30 */;
		echo ' ';
		echo LR\Filters::escapeHtmlText($weightStatus) /* line 30 */;
		echo '
</div>


</body>
</html>
';
	}
}
